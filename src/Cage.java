import java.util.ArrayList;

public class Cage {
private ArrayList<Dog> dogs;
public Cage()
    {dogs=new ArrayList<Dog>();}
public void add(Dog dog)
    { dogs.add(dog);}

public Dog remove(String name)
    {for(int i=0; i<dogs.size();i++)
        if(dogs.get(i).getName().equals(name))
            return dogs.remove(i);
        return null;}

    public String chaos ()
        {String temp="";
            for(Dog d : dogs)
                temp+=d.getName()+" ";
            return temp;
        }

    @Override
    public String toString() {
        String temp="";
        for(Dog d : dogs)
            temp+=d.toString()+"\n";
        return temp;
    }

    public ArrayList<Dog> getDogs() {
        return dogs;
    }
}
