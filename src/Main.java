public class Main {

    public static void main(String[] args)
    {
        Dog dog = new Dog("Spike");
        System.out.println(dog.getName() + " says " + dog.speak());
        Yorkshire yorkshire= new Yorkshire("Ramona");
        Labrador labrador= new Labrador("Giorgio","blu");
        System.out.println(yorkshire+" "+labrador);
        Cage cage= new Cage();
        cage.add(new Dog("marco"));
        cage.add(new Labrador("moises","verde"));
        cage.add(new Yorkshire("lillo"));
        cage.add(new Dog("giorgio"));
        cage.add(new Labrador("Alberico","bordeaux"));
        LabradorCage lc=new LabradorCage();
        lc.transfer(cage);
        System.out.println(lc);
    }
}
