/****************************************************************
 Dog.java

 A class that holds a dog's name and can make it speak.

****************************************************************/
public class Dog
{
    protected String name;
    private static int breedWeight;

    /**
     * Constructor - stores name
     * @param name
     */

    public Dog(String name)
    {
        this.name = name;
    }

    /**
     * Getter
     * @return dog's name
     */
    public String getName()
    {
        return name;
    }

    /**
     * Barking method
     * @return a string of the dog's comment on life
     */
    public String speak()
    {
        return "Woof";
    }

    public static int avgBreedWeight()
    {
        return breedWeight;

    }

    @Override
    public String toString() {
        return "I'm a beautiful dog called "+name;
    }
}