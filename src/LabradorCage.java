public class LabradorCage extends Cage{
    public LabradorCage()
        {super();}

    @Override
    public void add(Dog dog) {
        if(dog instanceof Labrador)
            super.add(dog);
    }

    public void transfer(Cage cage)
        {for(Dog d: cage.getDogs())
            add(d);}
}
